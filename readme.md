# Backend Task

## 1. Describe possible performance optimizations for your Code

- ### I think adding automated testing would improve my code.
- ### Basicly I tried as hard as possible to follow some rules of 'Clean Code' from Robert Cecil Martin which I am pretty sure that I failed on some occasions. Definitely, I need to hear other professionals opinions to keep improving my coding skills.

## 2. Which things could be done better, than you’ve done it?

- ### Well, I think there are a lot of things that could be done better, then I've done. Let me take an example. The Model Class (USER), I think i wrote a little too messy code there while adding annonymous classes.

- ### Basicly, I tried to structure my code for better scalability, and if I failed or succeded, I would like to hear it from you.

- ### I do think that I lacked to document the code.

- ### I think, I should have used laravel.
