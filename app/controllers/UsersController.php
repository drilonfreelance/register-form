<?php

namespace App\Controllers;

use App\Core\App;
use App\Models\User;
session_start();
class UsersController{
    
    public function personal_info(){
        return view('register/index');
    }

    public function index(){
        if($_COOKIE['step'] == '1'){
            return redirect('register/step2');
        }
        else if($_COOKIE['step'] == '2'){
            return redirect('register/step3');
        }
        else{
            return view('register/index');
        }
    }
    public function post_step1(){
        if(isset($_POST['address_step'])){
            setcookie('firstname', $_POST['firstname'], time() + (86400 * 30), "/");
            setcookie('lastname', $_POST['lastname'], time() + (86400 * 30), "/");
            setcookie('phone', $_POST['phone'], time() + (86400 * 30), "/");
            setcookie('step', '1', time() + (86400 * 30), "/");
            return redirect('register/step2');
        }
    }

    public function get_address(){
        return view('register/address');
    }

    public function post_step2(){
        if(isset($_POST['payment_step'])){
            setcookie('street', $_POST['street'], time() + (86400 * 30), "/");
            setcookie('house_number', $_POST['house_number'], time() + (86400 * 30), "/");
            setcookie('zip_code', $_POST['zip_code'], time() + (86400 * 30), "/");
            setcookie('city', $_POST['city'], time() + (86400 * 30), "/");
            setcookie('step', '2', time() + (86400 * 30), "/");
            return redirect('register/step3');
        }        
    }

    public function get_payment(){
        return view('register/payment');
    }

    public function store_user(){
        if(isset($_POST['finish'])){
            
            $args = array(
                'firstname' => $_COOKIE['firstname'],
                'lastname'  => $_COOKIE['lastname'],
                'phone'     => $_COOKIE['phone'],
                'street'    => $_COOKIE['street'],
                'house_number'  => $_COOKIE['house_number'],
                'zip'   => $_COOKIE['zip_code'],
                'city'  => $_COOKIE['city'],
                'owner' => $_POST['pi_acc_name'],
                'iban' => $_POST['iban']
            );

            $user = new User($args);

            App::get('database')->insert('users', array(
                'person_id' => $this->store_person_info($user->getPerson_info()),
                'contact_id' => $this->store_contact_info($user->getContact_info()),
                'address_id' => $this->store_address($user->getAddress()),
                'bank_info_id'  => $this->store_bank_info($user->getBank_info())
            ));
            $user->setUser_id(App::get('database')->last_inserted_id());
            
            $paymentkey = $this->post_payment_data_to_api(
                                        $user->getUser_id(),
                                        $user->getBank_info()->getIban(), 
                                        $user->getBank_info()->getOwner()
                                );
                                
            $this->store_payment_info(
                $user->setPayment_info(
                    $paymentkey,
                    $user->getUser_id()
                )
            );
            setcookie('step', '2', time() - (86400 * 30), "/");
            
            $_SESSION['payment_key'] = $paymentkey;
            return redirect('register/success');
        }
    }
    public function success(){
        $paymentkey = isset( $_SESSION['payment_key'] ) ? $_SESSION['payment_key'] : '';

        return view('register/success', compact('paymentkey'));
    }
    protected function store_payment_info($payment_info){
        App::get('database')->insert('payments', array(
            'gen_api_payment_id' => $payment_info->getPayment_key(),
            'user_id' => $payment_info->getUser_id(),
        ));
    }
    protected function store_person_info($person_info){
        App::get('database')->insert('customer_infos', array(
            'firstname' => $person_info->getFirstName(),
            'lastname' => $person_info->getLastName(),
        ));
        return App::get('database')->last_inserted_id();
    }
    protected function store_address($address){
        App::get('database')->insert('address', array(
            'street' => $address->getStreet(),
            'house_number' => $address->getHouse_number(),
            'zip' => $address->getZip(),
            'city' => $address->getCity(),
        ));
        return App::get('database')->last_inserted_id();
    }
    protected function store_contact_info($contact_info){
        App::get('database')->insert('contact_infos', array(
            'phone' => $contact_info->getPhone(),
        ));
        return App::get('database')->last_inserted_id();
    }
    protected function store_bank_info($bank_info){
        App::get('database')->insert('bank_infos', array(
            'owner' => $bank_info->getOwner(),
            'iban' => $bank_info->getIban(),
        ));
        return App::get('database')->last_inserted_id();
    }

    protected function post_payment_data_to_api($userid, $iban, $owner){
        $url = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';
        $ch = curl_init($url);
        $jsonData = array(
            'customerId' => $userid,
            'iban' => $iban,
            'owner' => $owner
        );
        $jsonDataEncoded = json_encode($jsonData);
        $args = array(
            CURLOPT_POST => 1, 
            CURLOPT_HTTPHEADER => array('Content-Type: application/json'), 
            CURLOPT_URL => $url, 
            CURLOPT_RETURNTRANSFER => 1, 
            CURLOPT_POSTFIELDS => $jsonDataEncoded
        );
        
        $ch = curl_init($url); 
        curl_setopt_array($ch, $args); 
        if( ! $result = curl_exec($ch)) 
        { 
            trigger_error(curl_error($ch));
        } 
        curl_close($ch);
        // return $result; 
        $response = json_decode($result, true);
        return $response['paymentDataId'];
    }
}