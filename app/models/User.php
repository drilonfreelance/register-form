<?php

namespace App\Models;

class User{

    private $user_id;
    private $person_info;
    private $address;
    private $bank_info;
    private $contact_info;
    private $payment_info;

    public function __construct($args){
        $this->person_info = $this->fill_person_info($args['firstname'], $args['lastname']);
        $this->address = $this->fill_address($args['street'], $args['house_number'], $args['zip'], $args['city']);
        $this->contact_info = $this->fill_contact_info($args['phone']);
        $this->bank_info = $this->fill_bank_info($args['owner'], $args['iban']);
    }

    protected function fill_person_info($firstname, $lastname){
        return new class($firstname, $lastname){
            private $firstname, $lastname;
            public function __construct($firstname, $lastname){
                $this->firstname = $firstname;
                $this->lastname = $lastname;
            }
            public function getFirstName(){
                return $this->firstname;
            }
            public function getLastName(){
                return $this->lastname;
            }            
        };
    }

    protected function fill_address($street, $house_number, $zip, $city){
        return new class($street, $house_number, $zip, $city){
            private $street, $house_number, $zip, $city;
            public function __construct($street, $house_number, $zip, $city){
                $this->street = $street;
                $this->house_number = $house_number;
                $this->zip = $zip;
                $this->city = $city;
            }
            public function getStreet() {
                return $this->street;
            }
            public function getHouse_number() {
                 return $this->house_number;
            }
            public function getZip() {
                return $this->zip;
            }
            public function getCity() {
                return $this->city;
            }
        };
    }

    protected function fill_contact_info($phone){
        return new class($phone) {
            private $phone;
            public function __construct($phone){
                $this->phone = $phone;
            }
            public function getPhone(){
                return $this->phone;
            }
        };
    }
    protected function fill_bank_info($owner, $iban){
        return new class($owner, $iban){
            private $owner, $iban;
            public function __construct($owner, $iban){
                $this->owner = $owner;
                $this->iban = $iban;
            }
            public function getOwner(){
                return $this->owner;
            }
            public function getIban(){
                return $this->iban;
            }
        };
    }

    protected function fill_payment_info($payment_key, $user_id){
        return new class($payment_key, $user_id){
            private $payment_key, $user_id;
            public function __construct($payment_key, $user_id){
                $this->payment_key = $payment_key;
                $this->user_id = $user_id;
            }
            public function getPayment_key() {
                return $this->payment_key;
            }
            public function getUser_id() {
                return $this->user_id;
            }
        };
    }

    public function getUser_id() {
        return $this->user_id;
    }
    public function setUser_id($user_id) {
        $this->user_id = $user_id;
    }
    public function getPerson_info() {
        return $this->person_info;
    }
    public function getAddress() {
        return $this->address;
    }
    public function getBank_info() {
        return $this->bank_info;
    }
    public function getContact_info() {
        return $this->contact_info;
    }
    public function getPayment_info() {
        return $this->payment_info;
    }
    public function setPayment_info($payment_key, $user_id) {
        $this->payment_info = $this->fill_payment_info($payment_key, $user_id);
        return $this->payment_info;
    }
}