<?php

$router->get('', 'PagesController@home');
if( isset($_COOKIE['step']) ):
    $router->get('register', 'UsersController@index');
    $router->get('register/contact-information', 'UsersController@personal_info');
else:
    $router->get('register', 'UsersController@personal_info');
endif;

$router->post('register/address', 'UsersController@post_step1');
$router->get('register/step2', 'UsersController@get_address');

$router->post('register/payment', 'UsersController@post_step2');
$router->get('register/step3', 'UsersController@get_payment');

$router->post('register/finish', 'UsersController@store_user');
$router->get('register/success', 'UsersController@success');