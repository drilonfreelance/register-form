<?php

require 'core/App.php';
require 'core/database/Connection.php';
require 'core/database/QueryBuilder.php';
require 'core/Router.php';
require 'core/Request.php';
require('app/controllers/PagesController.php');
require('app/models/User.php');
require('app/controllers/UsersController.php');

use App\Core\App;

App::bind('config', require 'config.php');
App::bind('database', new QueryBuilder(Connection::make(App::get('config')['database'])));

function view($name, $data = []){
    extract($data);
    return require "app/views/{$name}.view.php";
}
function redirect($path){
    header("Location: /{$path}");
}