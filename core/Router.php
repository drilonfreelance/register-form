<?php

namespace App\Core;

class Router{

    protected $routes = [
        'GET' => [],
        'POST' => []
    ];

    public static function load($file){

        $router = new static;
        require $file;
        return $router;

    }

    public function get($uri, $controller){
        $this->routes['GET'][$uri] = $controller;
    }

    public function post($uri, $controller){
        $this->routes['POST'][$uri] = $controller;
    }


    public function direct($uri, $method){
        if (array_key_exists($uri, $this->routes[$method])){
            return $this->callAction(
                ...explode('@', $this->routes[$method][$uri])
            );
        }
        throw new Exception('doesnt exist');
    }

    protected function callAction($controller, $action){

        $controller = "App\\controllers\\{$controller}";
        $controller = new $controller;
        
        if( method_exists($controller, $action) ){
            return $controller->$action();
        }

    }

}